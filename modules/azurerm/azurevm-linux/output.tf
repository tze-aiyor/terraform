# output "public_ip_fqdn" {
#  description = "The FQDN that maps to this Pubic IP"
#  value = "${data.azurerm_public_ip.public_ip.fqdn}"
# }

output "public_ip_id" {
  description = "The public IP ID of the instance"
  value       = "${azurerm_public_ip.public_ip.id}"
}

output "vm_id" {
  value = "${azurerm_virtual_machine.vm.id}"
}

output "public_ip_name" {
  value = "${azurerm_public_ip.public_ip.name}"
}

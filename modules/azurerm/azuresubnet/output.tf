output "subnet_id" {
  description = "The ids of subnets created inside the newly vNet"
  value       = "${azurerm_subnet.subnet.id}"
}

output "subnet_name" {
  description = "Name of the subnet"
  value = "${azurerm_subnet.subnet.name}"
}

output "subnet_resource_group" {
  description = "Name of the resource group that governs this subnet"
  value = "${azurerm_subnet.subnet.resource_group_name}"
}

output "subnet_vnet" {
  description = "Name of the VNET of which this subnet is part of"
  value = "${azurerm_subnet.subnet.virtual_network_name}"
}

output "subnet_address_prefix" {
  description = "The address prefix of this subnet"
  value = "${azurerm_subnet.subnet.address_prefix}"
}

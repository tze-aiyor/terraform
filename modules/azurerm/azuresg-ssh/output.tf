output "security_rule_id" {
  description = "Security rule ID"
  value       = "${azurerm_network_security_rule.security_rule.id}"
}

output "security_group_id" {
  description = "Security group ID"
  value       = "${azurerm_network_security_group.sg.id}"
}

output "security_rule_name" {
  description = "Security rule name"
  value       = "${azurerm_network_security_rule.security_rule.name}"
}

output "security_group_name" {
  description = "Security group name"
  value       = "${azurerm_network_security_group.sg.name}"
}

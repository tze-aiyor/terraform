# Create Network Security Group and rule
resource "azurerm_network_security_group" "sg" {
  name                = "${var.security_group_name}"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  tags = "${var.tags}"
}

resource "azurerm_network_security_rule" "security_rule" {
  name                        = "${var.security_rule_name}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.sg.name}"
  priority                    = 1001
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "${var.source_address_prefix}"
  destination_address_prefix  = "${var.destination_address_prefix}"
}

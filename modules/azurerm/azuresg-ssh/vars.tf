variable "resource_group_name" {
  description = "Default resource group name that the database will be created in."
}

variable "location" {
  description = "The location/region where the database and server are created. Changing this forces a new resource to be created."
  default     = ""
}

variable "security_group_name" {
  description = "Security group name"
}

variable "security_rule_name" {
  description = "Security rule name"
  default     = "SSH"
}

variable "source_address_prefix" {
  description = "Security rule source address prefix"
  default = "*"
}

variable "destination_address_prefix" {
  description = "Security rule destination address prefix"
  default = "*"
}

variable "tags" {
  description = "The tags to associate with your network and subnets."
  type        = "map"

  default = {
    tag1 = ""
    tag2 = ""
  }
}

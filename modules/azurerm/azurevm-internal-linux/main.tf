# Create network interface
resource "azurerm_network_interface" "vmnic" {
  name                      = "${var.vm_name}-vmnic"
  location                  = "${var.location}"
  resource_group_name       = "${var.resource_group_name}"
  network_security_group_id = "${var.security_group_id}"
  internal_dns_name_label = "${var.internal_dns_name_label}"

  ip_configuration {
    name                          = "${var.vm_name}-vmnic-ipconf"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "dynamic"
  }

  tags = "${var.tags}"
}

# Create virtual machine
resource "azurerm_virtual_machine" "vm" {
  name                  = "${var.vm_name}"
  location              = "${var.location}"
  resource_group_name   = "${var.resource_group_name}"
  network_interface_ids = ["${azurerm_network_interface.vmnic.id}"]
  vm_size               = "${var.vm_size}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true


  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true

  storage_os_disk {
    name              = "${var.vm_name}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"

    # disk_size_gb = "256"
  }
  storage_image_reference {
    publisher = "${var.vmimage_publisher}"
    offer     = "${var.vmimage_offer}"
    sku       = "${var.vmimage_sku}"
    version   = "${var.vmimage_version}"
  }
  os_profile {
    computer_name  = "${var.vm_name}"
    admin_username = "${var.os_username}"
    admin_password = "${var.os_password}"
  }
  # Password based auth
  os_profile_linux_config {
    disable_password_authentication = false
  }

  # Certificate based auth
  # os_profile_linux_config {
  #    disable_password_authentication = true
  #    ssh_keys {
  #        path     = "/home/azureuser/.ssh/authorized_keys"
  #        key_data = "ssh-rsa AAAAB3Nz{snip}hwhqT9h"
  #    }
  #}

  tags = "${var.tags}"
}

variable "resource_group_name" {
  description = "Default resource group name that the database will be created in."
}

variable "location" {
  description = "The location/region where the database and server are created. Changing this forces a new resource to be created."
  default     = ""
}

variable "vm_name" {
  description = "Name of the VM"
}

variable "vm_size" {
  default = "Standard_B2S"
}

variable "os_disk_size_gb" {
  default = ""
}

variable "vmimage_publisher" {
  default = "Canonical"
}

variable "vmimage_offer" {
  default = "UbuntuServer"
}

variable "vmimage_sku" {
  default = "16.04-LTS"
}

variable "vmimage_version" {
  default = "latest"
}

variable "os_username" {
  default = "osadmin"
}

variable "os_password" {
  default = "P@ssw0rd@123"
}

variable "subnet_id" {}

variable "security_group_id" {
  description = "Security group ID"
}

variable "internal_dns_name_label" {
  default = ""
}

variable "tags" {
  description = "The tags to associate with your network and subnets."
  type        = "map"

  default = {
    tag1 = ""
    tag2 = ""
  }
}

output "vm_id" {
  value = "${azurerm_virtual_machine.vm.id}"
}

output "vm_ip" {
  value = "${azurerm_network_interface.vmnic.private_ip_address}"
}

output "vm_internal_fqdn" {
  value = "${azurerm_network_interface.vmnic.internal_dns_name_label}"
}

variable "resource_group_name" {
  description = "Default resource group name that the database will be created in."
}

variable "location" {
  description = "The location/region where the database and server are created. Changing this forces a new resource to be created."
  default     = ""
}

variable "vnet_name" {
  description = "Azure VNET name"
}

variable "address_space" {
  description = "Default VNET adddress space"
  default     = "10.0.0.0/16"
}

variable "subnet_name" {
  description = "Default subnet name"
  default     = "default_subnet"
}

variable "subnet_prefix" {
  default = "10.0.1.0/24"
}

variable "tags" {
  description = "The tags to associate with your network and subnets."
  type        = "map"

  default = {
    tag1 = ""
    tag2 = ""
  }
}

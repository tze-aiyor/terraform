output "vnet_id" {
  description = "The id of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.id}"
}

output "vnet_name" {
  description = "The Name of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.name}"
}

output "vnet_location" {
  description = "The location of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.location}"
}

output "vnet_address_space" {
  description = "The address space of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.address_space}"
}

output "vnet_resource_group" {
  description = "The resource group of which the VNET is governed"
  value       = "${azurerm_virtual_network.vnet.resource_group_name}"
}

output "subnet_id" {
  description = "The ids of subnets created inside the newly vNet"
  value       = "${azurerm_subnet.subnet.id}"
}

output "subnet_name" {
  description = "Name of the subnet"
  value       = "${azurerm_subnet.subnet.name}"
}

output "subnet_resource_group" {
  description = "Name of the resource group that governs this subnet"
  value       = "${azurerm_subnet.subnet.resource_group_name}"
}

output "subnet_vnet" {
  description = "Name of the VNET of which this subnet is part of"
  value       = "${azurerm_subnet.subnet.virtual_network_name}"
}

output "subnet_address_prefix" {
  description = "The address prefix of this subnet"
  value       = "${azurerm_subnet.subnet.address_prefix}"
}

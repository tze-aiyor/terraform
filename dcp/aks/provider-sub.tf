provider "azurerm" {
    subscription_id = ""

    # Azure's Application ID
    client_id       = ""

    # Secret Key
    client_secret   = ""

    # Azure's Directory ID
    tenant_id       = ""
}

output "vnet_id" {
  description = "The id of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.id}"
}

output "vnet_name" {
  description = "The Name of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.name}"
}

output "vnet_location" {
  description = "The location of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.location}"
}

output "vnet_address_space" {
  description = "The address space of the newly created vNet"
  value       = "${azurerm_virtual_network.vnet.address_space}"
}

output "vnet_subnets" {
  description = "The ids of subnets created inside the newl vNet"
  value       = "${azurerm_subnet.subnet.*.id}"
}

output "public_ip" {
  description = "The public IP ID of the instance"
  value = "${data.azurerm_public_ip.public_ip.ip_address}"
}

# output "public_ip_fqdn" {
#  description = "The FQDN that maps to this Pubic IP"
#  value = "${data.azurerm_public_ip.public_ip.fqdn}"
# }

output "public_ip_id" {
  description = "The public IP ID of the instance"
  value = "${azurerm_public_ip.public_ip.id}"
}

output "vm_id" {
  value = "${azurerm_virtual_machine.vm.id}"
}

output "security_group_id" {
  value = "${azurerm_network_security_group.sg.id}"
}

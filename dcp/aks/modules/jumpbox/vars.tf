variable "resource_group_name" {
  description = "Default resource group name that the database will be created in."
}

variable "location" {
  description = "The location/region where the database and server are created. Changing this forces a new resource to be created."
  default = "Australia East"
}

variable "vnet_name" {
    description = "Azure VNET name"
}

variable "address_space" {
    description = "Default VNET adddress space"
    default = "10.0.0.0/16"
}

variable "subnet_name" {
  description = "Default subnet name"
  default = "default_subnet"
}

variable "subnet_prefix" {
  default = "10.0.1.0/24"
}

variable "public_ip_name" {
  default = ""
}

variable "security_group_name" {
  default = ""
}

variable "vmnic_name" {
  description = "Name for the VM's NIC"
}

variable "vm_name" {
  description = "Name of the VM"
}

variable "vm_size" {
  default = "Standard_B2S"
}

variable "vmimage_publisher" {
  default = "Canonical"
}

variable "vmimage_offer" {
  default = "UbuntuServer"
}

variable "vmimage_sku" {
  default = "16.04-LTS"
}

variable "vmimage_version" {
  default = "latest"
}

variable "os_username" {
  default = "osadmin"
}

variable "os_password" {
  default = "P@ssw0rd@123"
}

variable "tags" {
  description = "The tags to associate with your network and subnets."
  type        = "map"

  default = {
    tag1 = ""
    tag2 = ""
  }
}

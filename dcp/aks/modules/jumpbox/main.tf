resource "azurerm_resource_group" "rg" {
  name     = "${var.resource_group_name}"
  location = "${var.location}"
}


resource "azurerm_virtual_network" "vnet" {
    name                = "${var.vnet_name}"
    address_space       = ["${var.address_space}"]
    location            = "${var.location}"
    resource_group_name = "${azurerm_resource_group.rg.name}"

    tags = "${var.tags}"
}

# Create subnet
resource "azurerm_subnet" "subnet" {
    name                 = "${var.subnet_name}"
    resource_group_name  = "${azurerm_resource_group.rg.name}"
    virtual_network_name = "${azurerm_virtual_network.vnet.name}"
    address_prefix       = "${var.subnet_prefix}"
}

# Create public IPs
resource "azurerm_public_ip" "public_ip" {
    name                         = "${var.public_ip_name}"
    location                     = "${var.location}"
    resource_group_name          = "${azurerm_resource_group.rg.name}"
    public_ip_address_allocation = "dynamic"

    tags = "${var.tags}"
}


data "azurerm_public_ip" "public_ip" {
  name                = "${azurerm_public_ip.public_ip.name}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  depends_on          = ["azurerm_virtual_machine.vm"]
}


# Create Network Security Group and rule
resource "azurerm_network_security_group" "sg" {
    name                = "${var.security_group_name}"
    location            = "${var.location}"
    resource_group_name = "${azurerm_resource_group.rg.name}"

    security_rule {
        name                       = "RDP"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "3389"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = "${var.tags}"
}

# Create network interface
resource "azurerm_network_interface" "vmnic" {
    name                      = "${var.vmnic_name}"
    location                  = "${var.location}"
    resource_group_name       = "${azurerm_resource_group.rg.name}"
    network_security_group_id = "${azurerm_network_security_group.sg.id}"

    ip_configuration {
        name                          = "${var.vmnic_name}_ipconf"
        subnet_id                     = "${azurerm_subnet.subnet.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.public_ip.id}"
    }

    tags = "${var.tags}"
}




# Create virtual machine
resource "azurerm_virtual_machine" "vm" {
    name                  = "${var.vm_name}"
    location              = "${var.location}"
    resource_group_name   = "${azurerm_resource_group.rg.name}"
    network_interface_ids = ["${azurerm_network_interface.vmnic.id}"]
    vm_size               = "${var.vm_size}"

    # Uncomment this line to delete the OS disk automatically when deleting the VM
    # delete_os_disk_on_termination = true

    # Uncomment this line to delete the data disks automatically when deleting the VM
    # delete_data_disks_on_termination = true

    storage_os_disk {
        name              = "${var.vm_name}-osdisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
        # disk_size_gb = "256"
    }

    storage_image_reference {
        publisher = "${var.vmimage_publisher}"
        offer     = "${var.vmimage_offer}"
        sku       = "${var.vmimage_sku}"
        version   = "${var.vmimage_version}"
    }

    os_profile {
        computer_name  = "${var.vm_name}"
        admin_username = "${var.os_username}"
        admin_password = "${var.os_password}"
    }

    os_profile_windows_config {
      enable_automatic_upgrades = false
      provision_vm_agent = false
    }

    tags = "${var.tags}"
}

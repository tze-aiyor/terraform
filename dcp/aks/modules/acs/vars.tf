variable "resource_group_name" {
  description = "Default resource group name that the database will be created in."
}

variable "location" {
  description = "The location/region where the database and server are created. Changing this forces a new resource to be created."
  default = "Australia East"
}

variable "acs_name" {
  description = "Azure Storage Account Name"
}

variable "master_dns_prefix" {
  default = ""
}

variable "agent_dns_prefix" {
  default = ""
}

variable "agent_admin_username" {
  default = ""
}

variable "agent_ssh_key" {
  default = ""
}

variable "agent_pool_profile_name" {
  default = ""
}

variable "agent_vm_size" {
  default = "Standard_A0"
}

variable "azure_sp_client_id" {
  default = ""
}

variable "azure_sp_client_secret" {
  default = ""
}

variable "tags" {
  description = "The tags to associate with your network and subnets."
  type        = "map"

  default = {
    tag1 = ""
    tag2 = ""
  }
}

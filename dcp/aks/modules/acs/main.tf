resource "azurerm_resource_group" "rg" {
  name     = "${var.resource_group_name}"
  location = "${var.location}"
}

resource "azurerm_container_service" "acs" {
  name                   = "${var.acs_name}"
  location               = "${azurerm_resource_group.rg.location}"
  resource_group_name    = "${azurerm_resource_group.rg.name}"
  orchestration_platform = "Kubernetes"

  master_profile {
    count      = 1
    dns_prefix = "${var.master_dns_prefix}"
  }

  linux_profile {
    admin_username = "${var.agent_admin_username}"

    ssh_key {
      key_data = "${var.agent_ssh_key}"
    }
  }

  agent_pool_profile {
    name       = "${var.agent_pool_profile_name}"
    count      = 1
    dns_prefix = "${var.agent_dns_prefix}"
    vm_size    = "${var.agent_vm_size}"
  }

  service_principal {
    client_id     = "${var.azure_sp_client_id}"
    client_secret = "${var.azure_sp_client_secret}"
  }

  diagnostics_profile {
    enabled = false
  }

  tags = "${var.tags}"
}

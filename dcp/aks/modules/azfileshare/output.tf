output "storage_share_url" {
  description = "URL of Storage Share"
  value       = "${azurerm_storage_share.storage_share.url}"
}

output "storage_share_id" {
  description = "Azure Storage Share ID"
  value = "${azurerm_storage_share.storage_share.id}"
}

output "storge_acc_primary_key" {
  description = "Azure Storage account primary access key"
  value = "${azurerm_storage_account.storage_acc.primary_access_key}"
}

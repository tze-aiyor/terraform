resource "azurerm_storage_account" "storage_acc" {
  name                     = "${var.storage_account_name}"
  resource_group_name      = "${var.resource_group_name}"
  location                 = "${var.location}"
  account_tier             = "${var.storage_account_tier}"
  account_replication_type = "${var.storage_account_replication_type}"

  tags = "${var.tags}"
}

resource "azurerm_storage_share" "storage_share" {
  name = "${var.storage_share_name}"

  resource_group_name  = "${var.resource_group_name}"
  storage_account_name = "${azurerm_storage_account.storage_acc.name}"

  quota = "${var.storage_share_quota}"
}

variable "resource_group_name" {
  default = "dcp-test-rg"
}

variable "location" {
  default = "Australia East"
}

variable "vnet_name" {
  default = "dcp-test-vnet"
}


variable "security_group_name"{
  default = "dcp-jumpbox-rds-sg"
}

module "jumpbox01" {
  source = "modules/jumpbox"

  resource_group_name = "${var.resource_group_name}"
  location = "${var.location}"
  vnet_name = "${var.vnet_name}"
  security_group_name = "${var.security_group_name}"
  public_ip_name = "jumpbox01-pip"
  vmnic_name = "jumpbox01-vmnic"
  vm_name = "jumpbox01"
  vm_size = "Standard_D2s_v3"
  vmimage_publisher = "MicrosoftVisualStudio"
  vmimage_offer = "VisualStudio"
  vmimage_sku = "VS-2017-Comm-Latest-WS2016"
  os_username = "osadmin"
  os_password = "P@ssw0rd@123"
}

module "jumpbox02" {
  source = "modules/jumpbox"

  resource_group_name = "${var.resource_group_name}"
  location = "${var.location}"
  vnet_name = "${var.vnet_name}"
  security_group_name = "${var.security_group_name}"
  public_ip_name = "jumpbox02-pip"
  vmnic_name = "jumpbox02-vmnic"
  vm_name = "jumpbox02"
  vm_size = "Standard_D2s_v3"
  vmimage_publisher = "MicrosoftVisualStudio"
  vmimage_offer = "VisualStudio"
  vmimage_sku = "VS-2017-Comm-Latest-WS2016"
  os_username = "osadmin"
  os_password = "P@ssw0rd@123"
}

module "azure_sql_01" {
  source = "modules/azuresql"

  resource_group_name = "${var.resource_group_name}"
  location = "${var.location}"
  db_name = "dcp-db02"
  db_edition = "Basic"
  service_objective_name = "Basic"
  collation = "SQL_Latin1_General_CP1_CI_AS"
  db_server_name = "dcp-sql01"
  sql_admin_username = "dbadmin"
  sql_password = "P@ssw0rd@123"
  sql_fwrule_name = "dcp_sql_fw"
  start_ip_address = "0.0.0.0"
  end_ip_address = "0.0.0.0"
}

module "fileshare" {
  source = "modules/azfileshare"

  resource_group_name = "${var.resource_group_name}"
  location = "${var.location}"
  storage_account_name = "dcpstorageacc"
  storage_account_tier = "Standard"
  storage_account_replication_type = "LRS"
  storage_share_name = "dcp-files-share"
  storage_share_quota = 50
}
